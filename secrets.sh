#!/bin/bash -e
function does_secret_exist {
    if docker secret ls | grep -q "$1"; then
        echo " $1 exists, will not be re-adding to docker secrets."
        echo "ToDo: Clean up this code."
        return 0
    else
        echo "Adding Secret"
        echo $2 | docker secret create $1 -
        echo "$1 to docker secrets"
        does_secret_exist
    fi
}

export -f does_secret_exist

cat .secretnames | \
    parallel \
        -j $(nproc) \
        """
        does_secret_exist {} $"{}"
        """
