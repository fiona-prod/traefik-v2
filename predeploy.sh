#!/bin/sh
echo "### Cleaning Images"
docker image prune -af

echo "### Syncing config files"
#rsync -avshP --delete files/ /docker/traefik/
#touch /docker/traefik/acme/acme.json
#mkdir -p /tmp/traefik/logs
#touch /tmp/traefik/logs/traefik.log
#sudo chown -R 10101:10101 /tmp/traefik /docker/traefik
#sudo chmod 600 /docker/traefik/acme/acme.json
